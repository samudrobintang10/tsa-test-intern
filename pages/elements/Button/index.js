export default function Button(props) {
  const className = [props.className];
  let styling;
  if (props.isPrimary) {
    className.push("btn btn-primary");
    styling = {
      backgroundColor: "#4867B9",
      height: 56,
      width: "100%",
      borderRadius: 8,
    };
  }
  return (
    <button className={className} style={styling} onClick={props.onClick}>
      {props.children}
    </button>
  );
}
